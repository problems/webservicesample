﻿
using RestSharp;
using System.Configuration;
using System.Net;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WebServiceSample_AeC.Test
{
	[TestClass]
	public class RestSharpTest
	{
		private RestClient _restClient;
		private RestRequest _restRequest;

		[TestInitialize]
		public void SetUp()
		{
			var url = ConfigurationManager.AppSettings["url"];
			_restClient = new RestClient(url);
			_restRequest = new RestRequest("ConsultaFuncionarioRm", Method.POST);
			_restRequest.AddParameter("matricula", "123456");
		}

		[TestMethod]
		public void Should_return_200()
		{
			// Arrange

			// Act
			var result = _restClient.Execute(_restRequest);

			// Assert
			result.StatusCode.Should().Be(HttpStatusCode.OK);

		}

		[TestMethod]
		public void Should_data_strongly_not_null()
		{
			// Arrange

			// Act
			var result = _restClient.Execute<UserRM>(_restRequest);

			// Assert
			result.Data.Should().NotBeNull().And.BeOfType<UserRM>();

		}

		[TestMethod]
		public void Should_data_strongly_and_fill()
		{
			// Arrange

			// Act
			var result = _restClient.Execute<UserRM>(_restRequest);
			
			// Assert
			result.Data.Should().NotBeNull().And.BeOfType<UserRM>();
			result.Data.Nome.Should().NotBeNull();

		}
	}

	public class UserRM
	{
		public string Nome { get; set; }
		public string Email { get; set; }
		public string Matricula { get; set; }
	}
}
