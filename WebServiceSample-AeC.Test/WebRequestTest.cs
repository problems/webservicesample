﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Configuration;
using System.Xml;
using System;
using System.Linq;
using System.IO;
using FluentAssertions;
using System.Xml.Linq;
using System.Data;

namespace WebServiceSample_AeC.Test
{
	[TestClass]
	public class WebRequestTest
	{
		private string _url;

		[TestInitialize]
		public void SetUp()
		{
			_url = ConfigurationManager.AppSettings["url"];
			
		}

		[TestMethod]
		public void Should_result_not_null()
		{
			// Arrange
			var response = GetResponseWS();
			
			// Act
			var usr = ConvertFromStream(response.GetResponseStream());

			// Assert
			usr.Should().NotBeNull();
		}

		[TestMethod]
		public void Should_return_200()
		{
			// Act
			var response = GetResponseWS();

			// Assert
			response.StatusCode.Should().Be(HttpStatusCode.OK);
		}

		[TestMethod]
		public void Should_data_strongly_and_fill()
		{
			// Arrange
			var response = GetResponseWS();

			// Act
			var usr = ConvertFromStream(response.GetResponseStream());

			// Assert
			usr.Nome.Should().NotBeEmpty();
		}

		#region WS

		public HttpWebResponse GetResponseWS()
		{
			byte[] buffer = Encoding.ASCII.GetBytes(GetSoap11Envelope());
			//Initialization, webrequest
			HttpWebRequest WebReq =
			(HttpWebRequest)WebRequest.Create(_url);

			//Set the method/action type
			WebReq.Method = "POST";

			//We use form contentType
			WebReq.ContentType = "text/xml; charset=utf-8";

			//The length of the buffer
			WebReq.ContentLength = buffer.Length;

			//We open a stream for writing the post  data
			Stream MyPostData = WebReq.GetRequestStream();

			//Now we write, and after wards, we close. Closing is always important!
			MyPostData.Write(buffer, 0, buffer.Length);
			MyPostData.Close();

			//Get the response handle, we have no true response yet!
			return (HttpWebResponse)WebReq.GetResponse();
		}

		private string GetSoap11Envelope()
		{
			return @"<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
				<soap:Body> <ConsultaFuncionarioRm xmlns=""http://localhost:58592/""> <matricula>123456</matricula> </ConsultaFuncionarioRm> </soap:Body> </soap:Envelope>";
		}
		private string GetSoap12Envelope()
		{
			return @"<?xml version=""1.0"" encoding=""utf-8""?> <soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">
					<soap12:Body> <ConsultaFuncionarioRm xmlns=""http://localhost:58592/""> <matricula>123456</matricula> </ConsultaFuncionarioRm> </soap12:Body> </soap12:Envelope>";
		}

		private UserRM ConvertFromStream(Stream stream)
		{
			var ds = new DataSet();
			ds.ReadXml(new StreamReader(stream), XmlReadMode.ReadSchema);

			return new UserRM{ 
				Nome=ds.Tables[0].Rows[0]["NOME"].ToString()
				,Email=ds.Tables[0].Rows[0]["EMAIL"].ToString()
				,Matricula=ds.Tables[0].Rows[0]["CHAPA"].ToString()
			};
		}

		#endregion

	}
}
