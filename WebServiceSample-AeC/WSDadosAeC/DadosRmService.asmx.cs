﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Services;
using System.Web.Services;

namespace WebServiceSample_AeC.WSDadosAeC
{
	/// <summary>
	/// Summary description for DadosRmService
	/// </summary>
	[WebService(Namespace = "http://localhost:58592/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
	public class DadosRmService : System.Web.Services.WebService
	{

		public class UsuarioRM
		{
			public string Nome { get; set; }
			public string Email { get; set; }
			public string Matricula { get; set; }
		}

		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
		public DataSet ConsultaFuncionarioRm(string matricula)
		{
			var ds = new DataSet();
			var dt = new DataTable();
			dt.Columns.Add("CHAPA");
			dt.Columns.Add("NOME");
			dt.Columns.Add("CARGO");
			dt.Columns.Add("SITUACAO");
			dt.Columns.Add("EMAIL");
			
			var dr = dt.NewRow();
			dr["CHAPA"] = "083430";
			dr["NOME"] = "KLEBER FARIA SALES";
			dr["CARGO"] = "ANALISTA DE SISTEMAS VII";
			dr["SITUACAO"] = "ATIVO";
			dr["EMAIL"] = "kleber.sales@aec.com.br";
			dt.Rows.Add(dr);
			ds.Tables.Add(dt);

			return ds;
		}
	}
}
